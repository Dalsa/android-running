package com.example.samplelifecycle;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity2.class);
                startActivity(intent);
            }
        });

        println("onCreate 호출됨");
    }

    @Override
    protected void onStart() {
        super.onStart();

        println("onStrat 호출됨");
    }

    @Override
    protected void onStop() {
        super.onStop();

        println("onStop 호출됨");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        println("onDestory 호출됨");
    }

    @Override
    protected void onPause() {
        super.onPause();

        println("onPause 호출됨");

        saveState();
    }

    @Override
    protected void onResume() {
        super.onResume();

        println("onResume 호출됨");

        restoreState();
    }

    void println(String data) {
        Toast.makeText(this, data, Toast.LENGTH_LONG).show();
        Log.d("Main", data);
    }

    void saveState() {
        SharedPreferences pref = getSharedPreferences("pref", Activity.MODE_PRIVATE); //sharepreferencese 임시저장장소..
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("name", editText.getText().toString());
        editor.commit();
    }

    void restoreState() {
        SharedPreferences pref = getSharedPreferences("pref", Activity.MODE_PRIVATE);
        if ((pref != null) && (pref.contains("name"))) {
            String name = pref.getString("name", "");
            editText.setText(name);
        }
    }
}