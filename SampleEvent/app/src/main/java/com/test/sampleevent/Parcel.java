package com.test.sampleevent;

import android.os.Parcelable;

public class Parcel implements Parcelable {
    private String id;
    private String pw;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeString(this.pw);
        dest.writeString(this.id);
    }

    public Parcel() {
    }

    protected Parcel(android.os.Parcel in) {
        this.pw = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Parcel> CREATOR = new Parcelable.Creator<Parcel>() {
        @Override
        public Parcel createFromParcel(android.os.Parcel source) {
            return new Parcel(source);
        }

        @Override
        public Parcel[] newArray(int size) {
            return new Parcel[size];
        }
    };
}
