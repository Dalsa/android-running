package com.test.leedonghyeon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class LeeDongHyeonResAcitivity extends AppCompatActivity {
    EditText c1, c2, c3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leedonghyeonres_layout);

        c1 = (EditText) findViewById(R.id.jajjang_editText);
        c2 = (EditText) findViewById(R.id.jjambbong_editText);
        c3 = (EditText) findViewById(R.id.tang_editText);
    }

    public void onOrderFinished(View v) {
        int jajang, jjambbong, tang;

        jajang = Integer.parseInt(c1.getText().toString());
        jjambbong = Integer.parseInt(c2.getText().toString());
        tang = Integer.parseInt(c3.getText().toString());

        OrderData orderData = new OrderData(jajang,jjambbong,tang);

        Intent intent = new Intent();
        intent.putExtra("RESDATA", orderData);
        setResult(RESULT_OK, intent);

        finish();
    }

}
