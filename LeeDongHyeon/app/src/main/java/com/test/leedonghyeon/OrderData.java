package com.test.leedonghyeon;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderData implements Parcelable {
    int [] orders;

    public OrderData(int c1, int c2, int c3) {
        orders = new int[4];
        orders[0] = c1;
        orders[1] = c2;
        orders[2] = c3;
        orders[3] = c1 * 4000 + c2 * 5000 + c3 * 10000; // 총 금액
    }

    protected OrderData(Parcel in) {
        orders = new int[4];
        in.readIntArray(orders);
    }

    public static final Creator<OrderData> CREATOR = new Creator<OrderData>() {
        @Override
        public OrderData createFromParcel(Parcel in) {
            return new OrderData(in);
        }

        @Override
        public OrderData[] newArray(int size) {
            return new OrderData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeIntArray(orders);
    }
}
