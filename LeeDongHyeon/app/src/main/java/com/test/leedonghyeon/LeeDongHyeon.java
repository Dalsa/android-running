package com.test.leedonghyeon;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class LeeDongHyeon extends AppCompatActivity {

    final int REQUEST_CODE_GAL = 100;
    final int REQUEST_CODE_RES = 200;

    int [] drawableImages = {R.drawable.gal1, R.drawable.gal2, R.drawable.gal3, R.drawable.gal4,
            R.drawable.gal5};
    BitmapDrawable bitmap;
    ImageView imageView;
    TextView order, orderSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leedonghyeon_layout);

        imageView = (ImageView)findViewById(R.id.imageView);

        order = (TextView) findViewById(R.id.order_bill);
        orderSum = (TextView) findViewById(R.id.order);
    }

    public void onMyGal(View v) {
        Intent intent = new Intent(getApplication(), LeeDongHyeonGalActivity.class);
        startActivityForResult(intent, REQUEST_CODE_GAL);
    }

    public void onRes(View v) {
        Intent intent = new Intent(getApplication(), LeeDongHyeonResAcitivity.class);
        startActivityForResult(intent, REQUEST_CODE_RES);
    }

    public void changeImage(int index) {
        Resources res = getResources();
        bitmap = (BitmapDrawable) res.getDrawable(drawableImages[index]);
        imageView.setImageDrawable(bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_GAL) { //갤러리에서 끝나고 와서 왔을 경우
            Bundle bundle = data.getExtras();
            int selImageIndex = bundle.getInt("SELIMAGE");

            changeImage(selImageIndex);
        }
        else if (requestCode == REQUEST_CODE_RES) {
            Bundle bundle = data.getExtras();
            OrderData orderData = bundle.getParcelable("RESDATA");

            order.setText("짜장면 " + orderData.orders[0] + "그릇 " + orderData.orders[0]*4000+ "원\n" +
                    "짬뽕 " + orderData.orders[1] + "그릇 " + orderData.orders[1] * 5000 + "원\n" +
                    "탕수육 " + orderData.orders[2] + "그릇 " + orderData.orders[2] * 10000 + "원\n");

            orderSum.setText(Integer.toString(orderData.orders[3]));
        }
    }

}
