package com.test.leedonghyeon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class LeeDongHyeonGalActivity extends AppCompatActivity {
    ImageView galImageVIew;
    static int currentImage = 0;
    int [] drawableImages = {R.drawable.gal1, R.drawable.gal2, R.drawable.gal3, R.drawable.gal4,
            R.drawable.gal5};
    BitmapDrawable bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leedonghyeongal_layout);

        galImageVIew = (ImageView) findViewById(R.id.gal_imageVIew);
    }

    public void onBeforeImage(View v) {
        if(--currentImage == -1) {
            currentImage = 4;
            Toast.makeText(getApplicationContext(), "맨 처음 그림입니다.", Toast.LENGTH_LONG).show();
        }
        changeImage(currentImage);

    }

    public void onNextImage(View v){
        if (++currentImage == 5) {
            currentImage = 0;
            Toast.makeText(getApplicationContext(), "맨 마지막 그림입니다.", Toast.LENGTH_LONG).show();
        }
        changeImage(currentImage); //이미지 기능을 구현한다.
    }

    public void changeImage(int index) {
        Resources res = getResources();
        bitmap = (BitmapDrawable) res.getDrawable(drawableImages[index]);
        galImageVIew.setImageDrawable(bitmap);
    }

    public void onSelectImage(View v) {
        Intent intent = new Intent();
        intent.putExtra("SELIMAGE", currentImage);

        setResult(RESULT_OK, intent);

        finish();
    }
}
