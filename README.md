# android-running
------------------------------------------------------

안드로이드 네이티브 어플 프로그램 학습 연습합니다.

책의 내용을 실험적으로 공부하면서 책 제목은 'Do it 안드로이드 앱 프로그래밍'책 입니다.

제목이 일부분 일치하지 않을 수도 있는데, 제목에 따라서 어떻게 되는지 학습하기 위함이였으며,
Kotlin말고 자바를 기점으로 학습을 하는 프로젝트입니다.

자세한 설명을 달지 않고 코드 내용중 필요한 부분은 자유롭게 보셔도 상관없습니다.