package com.party_helper.hello;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity { //클래스의 시작

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButton1Clicked(View view) {
        Toast.makeText(this,"확인1 버튼이 눌렀어요.",Toast.LENGTH_LONG).show();
        //토스트 객체를 부르면
    }

    public void onButton2Clicked(View view) {
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://m.naver.com")); //Intent라는 객체를 만들어서
        startActivity(myIntent); //startActivity로 실생하기
    }

    public void onButton3Clicked(View view) {
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:010-5027-6384"));
        startActivity(myIntent);
    }
}
